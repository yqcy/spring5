import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.ipc.netty.http.server.HttpServer;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.*;

/**
 * created by wb-yq264139 on 2017/10/31
 */
public class Server {
    public static void main(String[] args) throws IOException {
        UserHandler userHandler = new UserHandler();
        RouterFunction<ServerResponse> route = nest(path("/user"),
                nest(accept(APPLICATION_JSON),
                        route(GET("/{username}"), userHandler::queryByUsername)));
        ReactorHttpHandlerAdapter adapter = new ReactorHttpHandlerAdapter(toHttpHandler(route));
        HttpServer server = HttpServer.create("localhost", 8989);
        server.newHandler(adapter).block();
        System.out.println("按任意键以退出！");
        System.in.read();
    }
}
