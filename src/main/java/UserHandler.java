import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;

/**
 * created by wb-yq264139 on 2017/10/31
 */
public class UserHandler {

    public Mono<ServerResponse> queryByUsername(ServerRequest request) {
        String username = String.valueOf(request.pathVariable("username"));

        User user = new User("yangqing","123");

        Mono<User> userMono = Mono.justOrEmpty(user);

        Mono<ServerResponse> notFound = ServerResponse.notFound().build();

        return userMono.flatMap(u -> ServerResponse.ok().contentType(APPLICATION_JSON).body(fromObject(u))).switchIfEmpty(notFound);
    }
}
